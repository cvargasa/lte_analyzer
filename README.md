LTE analyzer
========

The main goal of this project is to decode all the messages transmitted in clear text by the eNB, including system information, random access responses, and DCI messages addressed to UEs within the cell. This project is a modified version of srsRAN, an open source SDR 4G/5G software suite from Software Radio Systems (https://github.com/srsran/srsRAN).

The main modifications are:

* The adaptation of srsUE in a non-intrusive LTE analyzer
* The implementation of a new function that decodes the whole Downlink Control Information (DCI) messages
* The minimization of loss of subframes


**1) Installation**
---------------

The installation has been verified on Ubuntu 18.04 and Ubuntu 20.04.

Install dependencies:
```
sudo apt-get install build-essential cmake libfftw3-dev libmbedtls-dev libboost-program-options-dev libconfig++-dev libsctp-dev
```

Install RF front-end drivers:
  * UHD:                 https://github.com/EttusResearch/uhd
  * BladeRF:             https://github.com/Nuand/bladeRF

Our LTE analyzer has been tested with USRP B210 and bladeRFx40


Clone and build LTE analyzer: 
```
git clone https://gitlab.imt-atlantique.fr/cvargasa/lte_analyzer.git
cd lte_analyzer
mkdir traces
mkdir build
cd build
cmake ../
make
```


**2) Usage Instructions**
---------------

Set the CPU scaling governor to performance mode:
```
echo "performance" | sudo tee /sys/devices/system/cpu/cpu*/cpufreq/scaling_governor 
```

Launch the LTE analyzer:
```
cd lte_analyzer/config/
sudo ../build/srsue/src/srsue configuration_file.conf 
```
Configuration files are located in the ```/config``` directory. In the ```configuration_file.conf``` file, the parameter ```dl_earfcn``` must be configured according to the downlink EARFCN of the mobile network operator. 

To stop monitoring, press Ctrl+C 

Two CSV files will be created in the ```/traces``` directory:
* ```Information_date.csv```: this file contains the cell configuration information.
* ```Trace_date.csv```: this file contains all captured DCI control messages.

